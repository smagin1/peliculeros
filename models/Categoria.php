<?php

namespace agenda\Models;

use Illuminate\Database\Eloquent\Model;
use Slim\Exception\NotFoundException;

class Categoria extends Model
{
    protected $table = 'categoria';
    protected $fillable = ['tipo', 'tipourl'];
    public $timestamps = false;

    function pelis()
    {
        return $this->hasMany(Pelis::class, "categoria_id");
    }
}
