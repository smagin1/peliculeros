<?php

namespace agenda\Models;

use Illuminate\Database\Eloquent\Model;

class nomCategoria extends Model
{
    protected $table = 'pelis';
    protected $fillable = ['nompeli', 'director', 'intro', 'argumento', 'fotourl', 'categoria_id', 'nomurl', 'trailerurl'];
    public $timestamps = false;

    function pelis()
    {
        return $this->hasMany(Pelis::class, "nompeli");
    }
}
