<?php

namespace agenda\Models;

use Illuminate\Database\Eloquent\Model;

class Pelis extends Model
{
    protected $table = 'pelis';
    protected $fillable = ['nompeli', 'director', 'intro', 'argumento', 'fotourl', 'categoria_id', 'nomurl', 'trailerurl'];
    public $timestamps = false;
}
