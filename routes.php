<?php

use Slim\App;
use agenda\middleware\MiddlewareSession as MiddlewareSession;

require __DIR__ . '/controlers/ControlerResumen.php';
require __DIR__ . '/controlers/ControlerIniciarSesion.php';
require __DIR__ . '/controlers/ControlerCrearSession.php';
require __DIR__ . '/controlers/ControlerCerrarSession.php';
require __DIR__ . '/controlers/ControlerArticuloNuevo.php';
require __DIR__ . '/controlers/ControlerCategoria.php';
require __DIR__ . '/controlers/ControlerSubirArticulo.php';
require __DIR__ . '/controlers/ControlerEliminarArticulo.php';
require __DIR__ . '/controlers/ControlerPeli.php';
require __DIR__ . '/controlers/ControlerModificarArticulo.php';
require __DIR__ . '/controlers/ControlerArticuloModificado.php';

require __DIR__ . '/middlewares/MiddlewareSession.php';

require __DIR__ . '/models/Categoria.php';
require __DIR__ . '/models/Pelis.php';
require __DIR__ . '/models/nomCategoria.php';

return function (Slim\App $app, DI\Container $container) {

  $midSession = new MiddlewareSession($container->get("view"));

  $app->get('/', \ControlerResumen::class);
  $app->get('/sesion', \ControlerIniciarSesion::class);
  $app->get('/cerrarSession', \ControlerCerrarSession::class);
  $app->get('/articulos', \ControlerPeli::class)->add($midSession);
  $app->get('/articuloNuevo', \ControlerArticuloNuevo::class);
  $app->post('/crearSession', \ControlerCrearSession::class);
  $app->post('/subirArticulo', \ControlerSubirArticulo::class);
  $app->get('/categoria/{nomurl}', \ControlerCategoria::class);
  $app->delete('/articulo/{id}', \ControlerEliminarArticulo::class);
  $app->get('/articuloModificar/{nompeli}', \ControlerModificarArticulo::class);
  $app->post('/modificadoArticulo', \ControlerArticuloModificado::class);
};
