**    Informacion para instalar correctamente el proyecto PELICULEROS     **

Al descargarnos este repositorio veremos que nos descarga un archivo llamado "projecto_peliculas_.sql", este script deberemos importarlo en PHPMyAdmin.

Para una correcto funcionamiento de Slim crearemos un virtual host en apache, para ello deberemos hacer los siguientes pasos:

        - Añadir al fichero "xampp\apache\conf\extra\httpd-vhost.conf" el siguiente codigo
        
                <VirtualHost *:80>
                DocumentRoot "C:\xampp\htdocs\peliculeros-peliculeros\public"
                ServerName peliculeros.com
                </VirtualHost>

        - Añadir al fichero "C:\Windows\System32\drivers\etc" el siguiente codigo
        
                127.0.0.1 peliculeros.com

Ahora buscamos en el naveador, peliculeros.com y nos mostrara el proyecto
