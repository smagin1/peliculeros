<?php

use agenda\Models\Categoria as Categoria;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;


class ControlerCategoria
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        $busqueda = $args['nomurl'];
        $cat = Categoria::where("tipourl", $busqueda)->get()[0];
        $data = ["pelis" => $cat->pelis, "categoria" => strtoupper($busqueda)];

        return $this->container->get("view")->render($response, "categoria.html.twig", $data);
    }
}
