<?php

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use agenda\Models\Pelis as Pelis;

class ControlerSubirArticulo
{
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function __invoke(ServerRequestInterface $request, ResponseInterface $response, array $args): ResponseInterface
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $datos = $request->getParsedBody();
            $datos['fotourl'] = "/img/" . $_FILES['fotourl']['name'];
            $datos['nomurl'] = strtolower($datos['nompeli']);
            $datos['intro'] = $datos['introduccion'];
            unset($datos['introduccion']);
            $out = ["error" => false, 'message' => 'Articulo creado.'];
            try {
                $articulo = Pelis::Create($datos);
                move_uploaded_file($_FILES['fotourl']['tmp_name'], "img/" . $_FILES['fotourl']['name']);
                $out['articulo'] = $articulo;
            } catch (\Exception $e) {
                $out['error'] = true;
                $out['message'] = $e->getMessage();
            }
            $response->getBody()->write(json_encode($out));
        }
        return $response->withHeader('Content-Type', 'application/json');
    }
}
