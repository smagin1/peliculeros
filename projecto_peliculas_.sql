-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-05-2021 a las 21:28:15
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `projecto_peliculas`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL,
  `tipo` varchar(50) DEFAULT NULL,
  `tipourl` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id`, `tipo`, `tipourl`) VALUES
(1, 'Acción', 'accion'),
(2, 'Dramáticas', 'dramaticas'),
(3, 'Terror', 'terror'),
(4, 'Comedias', 'comedias');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pelis`
--

CREATE TABLE `pelis` (
  `id` int(11) NOT NULL,
  `nompeli` varchar(50) DEFAULT NULL,
  `director` varchar(50) DEFAULT NULL,
  `intro` varchar(150) DEFAULT NULL,
  `argumento` varchar(700) DEFAULT NULL,
  `fotourl` varchar(300) DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  `nomurl` varchar(300) DEFAULT NULL,
  `trailerurl` varchar(290) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `pelis`
--

INSERT INTO `pelis` (`id`, `nompeli`, `director`, `intro`, `argumento`, `fotourl`, `categoria_id`, `nomurl`, `trailerurl`) VALUES
(1, 'El hombre sin pasado\n\r\n ', 'Lee Jeong-beom', 'Descubre este thriller de acción, sobradamente uno de los mejores y potentes títulos de la cinematografía surcoreana y realizada por el actor Won Bin.', 'La única conexión del ex-agente especial Cha Tae-sik con el mundo es una niña que vive cerca de su casa. La madre de la pequeña trabaja para una organización mafiosa que se dedica al tráfico de drogas y le confía a él la mercancía. Cuando los traficantes se dan cuenta, secuestran a la madre y a la niña. Además, confunden a Tae-sik con un camello.', '/img/el_hombre_sin_pasado.jpg', 1, 'el hombre sin pasado', 'https://www.youtube.com/embed/N5GKTN9oxVw'),
(2, 'Misión imposible: Protocolo fantasma', 'Brad Bird', 'Descubre la 4rta. entrega de la saga Misión Imposible donde la historia promete nuevas raciones de intriga y acción de la mano del agente Ethan Hunt.', 'Cuarta entrega de la serie cinematográfica Misión imposible. El agente Ethan Hunt, acusado de un atentado terrorista con bombas contra el Kremlin, es desautorizado junto con toda la organización, al poner en marcha el Presidente el “Protocolo Fantasma”. Abandonado a su suerte y sin recursos, el objetivo de Ethan es rehabilitar el buen nombre de su agencia e impedir un nuevo ataque. Pero Ethan emprende esta misión con un equipo formado por fugitivos, cuyos motivos personales no conoce bien.', '/img/mision_imposible.jpg', 1, 'mision imposible protocolo fantasma', 'https://www.youtube.com/embed/9MMPQ88NDWY'),
(3, 'Jack Reacher', 'Cristopher McQuarrie', 'Un expolicía militar que investiga a un francotirador acusado de 5 homicidios se ve envuelto en un peligroso juego del gato y el ratón.', 'Jack Reacher (Tom Cruise), un antiguo policía militar que vive como un vagabundo y que trabaja por su cuenta, decide investigar el caso de un francotirador que ha sido acusado de matar a cinco personas en un tiroteo. Al ser interrogado, el francotirador exige la presencia de Jack Reacher.', '/img/jack_reacher.jpg', 1, 'jack reacher', 'https://www.youtube.com/embed/0B-Z8pb6wYI'),
(4, 'Skyfall', 'Sam Mendes', 'James Bond el agente 007 deberá encontrar y destruir la amenaza, independientemente del coste personal que tenga que pagar. ¿Te atreves a descubrirlo?', 'La lealtad de James Bond (Daniel Craig), el mejor agente de los servicios secretos británicos, por su superiora M (Judi Dench) se verá puesta a prueba cuando episodios del pasado de ella vuelven para atormentarla. Al mismo tiempo, el MI6 sufre un ataque, y 007 tendrá que localizar y destruir el grave peligro que representa el villano Silva (Javier Bardem). Para conseguirlo contará con la ayuda de la agente Eve (Naomie Harris).', '/img/skyfall.jpg', 1, 'skyfall', 'https://www.youtube.com/embed/UbC7iRA_O3g'),
(5, 'Siempre a tu lado\n Hachiko', 'Lasse Hallström', 'Basada en un hecho real, Hachiko un perro que tras la muerte de su amo, estuvo durante 9 años esperándole en la estación donde su dueño cogía el tren.', 'Parker Wilson, un profesor universitario que da clases de música, recoge un día a un perro de origen japonés y raza Akita, al que encuentra abandonado en una estación. Como nadie lo reclama, se lo lleva a su casa. Parker va descubriendo entonces los entrañables lazos que pueden unir a una persona y a un animal. Remake de la película japonesa \"Hachiko monogatari\" (1987) dirigida por Seijirô Kôyama y basada en la historia real de un perro tan fiel a su dueño que iba todos los días a esperarlo a la estación. Actualmente, en esa estación, existe una estatua de bronce erigida en su honor.', '/img/siempre_hachiko.jpg', 2, 'siempre a tu lado hachiko', 'https://www.youtube.com/embed/jrVVIaO_hTg'),
(6, 'La vida es bella', 'Roberto Benigni', 'Dirigido por Roberto Benigni la película muestra la crueldad de la vida y, logra arrojar algo de luz y una visión de la belleza del amor y la vida. ', 'En 1939, a punto de estallar la Segunda Guerra Mundial (1939-1945), el extravagante Guido llega a Arezzo, en la Toscana, con la intención de abrir una librería. Allí conoce a la encantadora Dora y, a pesar de que es la prometida del fascista Rodolfo, se casa con ella y tiene un hijo. Al estallar la guerra, los tres son internados en un campo de exterminio, donde Guido hará lo imposible para hacer creer a su hijo que la terrible situación que están padeciendo es tan sólo un juego.', '/img/vida_bella.jpg', 2, 'la vida es bella', 'https://www.youtube.com/embed/e-Z062tRdbQ'),
(7, 'Intocable', 'Olivier Nakache, Eric Toledano', 'Es una comedia \"limpia\", aunque uno de sus protagonistas sea un tetrapléjico y se hagan algunas bromas nada subidas de tono al respecto. !Disfrútala!', 'Philippe, un aristócrata millonario que se ha quedado tetrapléjico a causa de un accidente de parapente, contrata como cuidador a domicilio a Driss, un inmigrante de un barrio marginal recién salido de la cárcel. Aunque, a primera vista, no parece la persona más indicada, los dos acaban logrando que convivan Vivaldi y Earth Wind and Fire, la elocuencia y la hilaridad, los trajes de etiqueta y el chándal. Dos mundos enfrentados que, poco a poco, congenian hasta forjar una amistad tan disparatada, divertida y sólida como inesperada, una relación única en su especie de la que saltan chispas.', '/img/intocable.jpg', 2, 'intocable', 'https://www.youtube.com/embed/8Gg0w81fe5E'),
(8, 'El niño con el pijama de rayas', 'Mark Herman', 'Película sobre el holocausto y la relación de dos niños, de vidas completamente opuestas, y cómo crece su amistad completamente ajena a la realidad.', 'Berlín, 1942. Bruno (Asa Butterfield) tiene ocho años y desconoce el significado de la Solución Final y del Holocausto. No es consciente de las pavorosas crueldades que su país, en plena guerra mundial, está infligiendo a los pueblos de Europa. Todo lo que sabe es que su padre -recién nombrado comandante de un campo de concentración- ha ascendido en el escalafón, y que ha pasado de vivir en una confortable casa de Berlín a una zona aislada. Todo cambia cuando conoce a Shmuel, un niño judío que vive una extraña existencia paralela al otro lado de la alambrada.', '/img/nino_rayas.jpg', 2, 'el nino con el pijama de rayas', 'https://www.youtube.com/embed/rzow19gyNqQ'),
(9, 'La bruja', 'Robert Eggers', '‘La bruja’ es una magnífica obra de terror, pero lo es de una forma más sigilosa y pausada en lugar de buscar el sobresalto a traición. ¿Te atreves?', 'Nueva Inglaterra, 1630. Un matrimonio de colonos cristianos, con cinco hijos, vive cerca de un bosque que, según las creencias populares, está dominado por el mal. Cuando el hijo recién nacido desaparece y los cultivos no crecen, los miembros de la familia se rebelan los unos contra los otros: un mal sobrenatural les acecha en el bosque cercano.', '/img/bruja.jpg', 3, 'la bruja', 'https://www.youtube.com/embed/l31gT5GIvao'),
(10, 'Babadook', 'Jennifer Kent', 'Monstruo de un cuento infantil traspasa las fronteras de la ficción para acechar a una mujer y su hijo, abordar los miedos infantiles. !!Te atreves!!', 'Seis años después de la violenta muerte de su marido, Amelia no se ha recuperado todavía, pero tiene que educar a Samuel, su hijo de 6 años, que vive aterrorizado por un monstruo que se le aparece en sueños y amenaza con matarlos. Cuando un inquietante libro de cuentos llamado “The Babadook” aparece en su casa, Samuel llega al convencimiento de que el Babadook es la criatura con la que ha estado soñando. Entonces sus alucinaciones se hacen incontrolables y su conducta, impredecible y violenta. Amelia, cada vez más asustada, se ve forzada a medicarle. Pero, de repente, empieza a sentir a su alrededor una presencia siniestra que la lleva a pensar que los temores de su hijo podrían ser reales.', '/img/babadook.jpg', 3, 'babadook', 'https://www.youtube.com/embed/29qrvJMui0c'),
(11, 'Expediente Warren: The Conjuring', 'James Wan', 'Película de terror explica la historia de una familia, los Perron, que se trasladan a una casa en el campo, allí vivirán aterradoras experiencias. ', 'Basada en una historia real documentada por los reputados demonólogos Ed y Lorraine Warren. Narra los encuentros sobrenaturales que vivió la familia Perron en su casa de Rhode Island a principios de los 70. El matrimonio Warren, investigadores de renombre en el mundo de los fenómenos paranormales, acudieron a la llamada de esta familia aterrorizada por la presencia en su granja de un ser maligno.', '/img/expediente_warren.jpg', 3, 'expediente warren', 'https://www.youtube.com/embed/1kOlrEwTfco'),
(12, 'El Exorcista', 'William Friedkin', 'Adaptación de la novela de William Peter Blatty se inspiró en un exorcismo real ocurrido en Washington en 1949. Regan es la víctima de esos fenómenos.', 'En Iraq, el Padre Merrin queda profundamente turbado por el descubrimiento de una figurilla del demonio Pazuzu y las macabras visiones que ésta provoca. Mientras tanto, en Washington, en la casa de la actriz Chris MacNeil se están produciendo extraños fenómenos: la despiertan extraños sonidos que vienen del granero y su hija Regan se queja de que su cama se mueve. \r\nLas crisis se hacen cada vez más frecuentes. Presa de violentos espasmos, la adolescente se vuelve irreconocible y tras realizarle diversas pruebas médicas, de las que no sacan ninguna conclusión, Chris decide acudir a un exorcista.', '/img/exorcista.jpg', 3, 'el exorcista', 'https://www.youtube.com/embed/7iCJssW8XiI'),
(13, 'Solo en casa', 'Chris Columbus', 'Kevin McAllister es un niño de 8 años, miembro de una familia numerosa, que accidentalmente queda abandonado en su casa. ¿Qué ocurrirá con este niño?', 'Kevin McAllister es un niño de ocho años, miembro de una familia numerosa, que accidentalmente se queda abandonado en su casa cuando toda la familia se marcha a pasar las vacaciones a Francia. Kevin aprende a valerse por sí mismo e incluso a protegerse de Harry y Marv, dos bribones que se proponen asaltar todas las casas cerradas de su vecindario. En cuanto su madre lo echa en falta, regresa apresuradamente a Chicago para recuperar a su hijo.', '/img/solo_casa.jpg', 4, 'solo en casa', 'https://www.youtube.com/embed/4nXRb-1-n60'),
(14, 'La boda de mi mejor amiga', 'Paul Feig', 'Annie 30 años, soltera, vida sentimental casi nula, con este panorama, que ocurrirá cuando su amiga le pida ser dama de honor. ¿Lo descubrimos?', 'Annie (Kristen Wiig) es una treintañera soltera del Medio Oeste, con una vida sentimental más bien precaria, a la que Lilliam, su mejor amiga (Maya Rudolph), le pide que sea su dama de honor. Sin embargo, aunque nunca ha ejercido esa función, la pobre se esfuerza por dárselas de snob en la fiesta anterior a la boda. Mientras tanto, otra amiga de Lillian (Rose Byrne) hará todo lo posible por arrebatarle el papel a la inexperta Annie. ', '/img/boda_amiga.jpg', 4, 'la boda de mi mejor amiga', 'https://www.youtube.com/embed/NQuoFfRpdSo'),
(15, 'Dos buenos tipos', 'Shane Black', 'Comedia ubicada en los años 70, llena de acción entre 2 compañeros tendrán que hacer todo lo que esté en su poder para salvar sus vidas.', 'Ambientada en Los Ángeles durante los años 70. El detective Holland March (Ryan Gosling) y el matón a sueldo Jackson Healy (Russell Crowe) se ven obligados a colaborar para resolver varios casos: la desaparición de una joven, la muerte de una estrella porno y una conspiración criminal que llega hasta las altas esferas.', '/img/dos_tipos.jpg', 4, 'dos buenos tipos', 'https://www.youtube.com/embed/mCfyTulhO_4'),
(16, 'Lady Bird', 'Greta Gerwig', 'Christine es una adolescente que se muda al norte de California para cursar allí su último año de instituto. Su sueño es llegar a ser una artista.', 'Christine (Saoirse Ronan), que se hace llamar \"Lady Bird\", es una adolescente de Sacramento en su último año de instituto. La joven, con inclinaciones artísticas y que sueña con vivir en la costa Este, trata de ese modo encontrar su propio camino y definirse fuera de la sombra protectora de su madre (Laurie Metcalf).', '/img/lady_bird.jpg', 4, 'lady bird', 'https://www.youtube.com/embed/4fDOaCQU2LU'),
(17, '28 Días Después', 'Danny Boyle', 'Lo más terrorífico de \"28 días después\" no son las acciones de los zombis, sino las intenciones de los hombres. ¡¡¡Te atreves a conocer la realidad!!', 'Londres es un cementerio. Las calles antes abarrotadas están ahora desiertas. Las tiendas, vacías. Y reina un silencio total. Tras la propagación de un virus que acabó con la mayor parte de la población de Gran Bretaña, tuvo lugar la invasión de unos seres terroríficos. El virus se difundió, tras la incursión en un laboratorio, de un grupo de defensores de los derechos de los animales. Transmitido a través de la sangre, el virus produce efectos devastadores en los afectados. En 28 días la epidemia se extiende por todo el país y sólo queda un puñado de supervivientes.', '/img/28_dias_despues.jpg', 3, '28 dias despues', 'https://www.youtube.com/embed/65HRLo6sz1w'),
(18, 'El silencio de los corderos', 'Jonathan Demme', 'Descubre la relación entre la agente Clarice y el Dr. Hannibal Lecter un psicoanalista y asesino, han de resolver el caso de un asesino en serie.', 'El FBI busca a \"Buffalo Bill\", un asesino en serie que mata a sus víctimas, todas adolescentes, después de prepararlas minuciosamente y arrancarles la piel. Para poder atraparlo recurren a Clarice Starling, una brillante licenciada universitaria, experta en conductas psicópatas, que aspira a formar parte del FBI. Siguiendo las instrucciones de su jefe, Jack Crawford, Clarice visita la cárcel de alta seguridad donde el gobierno mantiene encerrado al Dr. Hannibal Lecter, antiguo psicoanalista y asesino, dotado de una inteligencia superior a la normal. Su misión será intentar sacarle información sobre los patrones de conducta del asesino que están buscando.', '/img/silencio_de_los_corderos.jpg', 3, 'el silencio de los corderos', 'https://www.youtube.com/embed/tU_PuI591Uk'),
(19, 'El caballero oscuro: La leyenda renace', 'Christopher Nolan', 'Después de años sin aparecer Batman vuelve convertido en un fugitivo, culpado por la muerte del fiscal Harvey Dent, y la aparición de una ladrona. ', 'Hace ocho años que Batman desapareció, dejando de ser un héroe para convertirse en un fugitivo. Al asumir la culpa por la muerte del fiscal del distrito Harvey Dent, el Caballero Oscuro decidió sacrificarlo todo por lo que consideraba, al igual que el Comisario Gordon, un bien mayor. La mentira funciona durante un tiempo, ya que la actividad criminal de la ciudad de Gotham se ve aplacada gracias a la dura Ley Dent. Pero todo cambia con la llegada de una astuta gata ladrona que pretende llevar a cabo un misterioso plan. Sin embargo, mucho más peligrosa es la aparición en escena de Bane, un terrorista enmascarado cuyos despiadados planes obligan a Bruce a regresar de su voluntario exilio.', '/img/el_caballero_oscuro.jpg', 1, 'el caballero oscuro: la leyenda renace', 'https://www.youtube.com/embed/9MEuGQtM9wA'),
(20, 'The Matrix', 'Lilly Wachowski, Lana Wachowski', 'Nada es lo que parece. Las máquinas pueden apagar el espíritu humano: no debemos olvidar la grandeza y, ¡oh, paradoja!, la pequeñez del hombre.', 'Final de milenio. Neo es un profesional de la informática que un día tiene la oportunidad de conocer a Morpheus, un mítico personaje para cualquier cibernauta que se precie. Él le puede introducir en un mundo inesperado, cuya existencia jamás se le ocurrió imaginar. Pero basta que Morpheus intente el contacto para que Neo empiece a sufrir una persecución implacable.', '/img/matrix.jpg', 1, 'the matrix', 'https://www.youtube.com/embed/vN_Hx_It3r0'),
(21, 'Mejor imposible', 'James L. Brooks', 'Un escritor maniático con un trastorno obsesivo-compulsivo, Carol la camarera madre soltera y un artista gay como vecino. ¿¿Que ocurrirá con ellos??', 'Melvin Udall (Jack Nicholson), un escritor maniático que padece un trastorno obsesivo-compulsivo, es el ser más desagradable y desagradecido que uno pueda tener como vecino en Nueva York. Entre sus rutinas está la de comer todos los días en una cafetería, donde le sirve Carol Connelly (Helen Hunt), camarera y madre soltera. Simon Nye (Greg Kinnear), un artista gay que vive en el apartamento contiguo al de Melvin, sufre constantemente su homofobia. De repente, un buen día, Melvin tiene que hacerse cargo de un pequeño perro aunque detesta los animales. La compañía del animal contribuirá a suavizar su carácter.', '/img/mejor_imposible.jpg', 4, 'mejor imposible', 'https://www.youtube.com/embed/dlLKo-_slWg'),
(22, 'Resacón en Las Vegas', 'Todd Phillips', 'Disparatada despedida de solteros de 4 amigos. Siempre puede ir a peor las cosas cuando no te acuerdas de la noche anterior. !Brutal y Alocada noche!', 'Historia de una desmadrada despedida de soltero en la que el novio y tres amigos se montan una gran juerga en Las Vegas. Como era de esperar, a la mañana siguiente tienen una resaca tan monumental que no pueden recordar nada de lo ocurrido la noche anterior. Lo más extraordinario es que el novio ha desaparecido y en la suite del hotel se encuentran un tigre y un bebé.', '/img/resacon_en_las_vegas.jpg', 4, 'resacon en Las vegas', 'https://www.youtube.com/embed/HsK23vU-65A'),
(23, 'La lista de Schindler', 'Steven Spielberg', 'Empresario alemán que se aprovecha de su talento para obtener la simpatía de los nazis para su beneficio personal. ¿Qué hará para salvar a los judíos?', 'Oskar Schindler, un empresario alemán de gran talento para las relaciones públicas, busca ganarse la simpatía de los nazis de cara a su beneficio personal. Después de la invasión de Polonia por los alemanes en 1939, Schindler consigue, gracias a sus relaciones con los altos jerarcas nazis, la propiedad de una fábrica de Cracovia. Allí emplea a cientos de operarios judíos, cuya explotación le hace prosperar rápidamente, gracias sobre todo a su gerente Itzhak Stern (Ben Kingsley), también judío. Pero conforme la guerra avanza, Schindler y Stern comienzan ser conscientes de que a los judíos que contratan, los salvan de una muerte casi segura en el temible campo de concentración de Plaszow.', '/img/schindler.jpg', 2, 'la lista de schindler', 'https://www.dailymotion.com/embed/video/x70c4l3'),
(24, 'Million Dollar Baby', 'Clint Eastwood', 'Frankie Dunn regenta un gimnasio con la ayuda de Scrap, un ex-boxeador que es su único amigo. Maggie una voluntariosa chica que quiere boxear. ', 'Después de haber entrenado y representado a los mejores púgiles, Frankie Dunn (Eastwood) regenta un gimnasio con la ayuda de Scrap (Freeman), un ex-boxeador que es además su único amigo. Frankie es un hombre solitario y adusto que se refugia desde hace años en la religión buscando una redención que no llega. Un día, entra en su gimnasio Maggie Fitzgerald (Swank), una voluntariosa chica que quiere boxear y que está dispuesta a luchar denodadamente para conseguirlo. Frankie la rechaza alegando que él no entrena chicas y que, además, es demasiado mayor. Pero Maggie no se rinde y se machaca cada día en el gimnasio, con el único apoyo de Scrap.', '/img/million_dollar_baby.jpg', 2, 'million dollar baby', 'https://www.youtube.com/embed/_iH47yRY9UM');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pelis`
--
ALTER TABLE `pelis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_categoria_id` (`categoria_id`) USING BTREE;

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `pelis`
--
ALTER TABLE `pelis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `pelis`
--
ALTER TABLE `pelis`
  ADD CONSTRAINT `FK_categoriaid` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
